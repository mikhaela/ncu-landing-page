﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using LandingPage.Data;

namespace LandingPage.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("LandingPage.Color", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Value")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Colors");
                });

            modelBuilder.Entity("LandingPage.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("LandingPage.Models.Banner", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("BackgroundColorId");

                    b.Property<int?>("BackgroundImageId");

                    b.Property<int>("HeadSectionId");

                    b.Property<string>("Heading");

                    b.Property<int>("Order");

                    b.Property<string>("Subheading");

                    b.Property<bool>("Visible");

                    b.HasKey("Id");

                    b.HasIndex("BackgroundColorId");

                    b.HasIndex("BackgroundImageId");

                    b.HasIndex("HeadSectionId");

                    b.ToTable("Banners");
                });

            modelBuilder.Entity("LandingPage.Models.Blog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ButtonId");

                    b.Property<string>("Description");

                    b.Property<int>("FooterId");

                    b.Property<int>("HeaderId");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.HasIndex("ButtonId")
                        .IsUnique();

                    b.HasIndex("FooterId")
                        .IsUnique();

                    b.HasIndex("HeaderId")
                        .IsUnique();

                    b.ToTable("Blogs");
                });

            modelBuilder.Entity("LandingPage.Models.BrandSection", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Heading");

                    b.Property<int>("SectionId");

                    b.Property<string>("Subheading");

                    b.HasKey("Id");

                    b.HasIndex("SectionId")
                        .IsUnique();

                    b.ToTable("BrandSections");
                });

            modelBuilder.Entity("LandingPage.Models.Button", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Action");

                    b.Property<int>("ColorId");

                    b.Property<string>("LabelForButton");

                    b.Property<string>("Value");

                    b.Property<bool>("Visible");

                    b.HasKey("Id");

                    b.HasIndex("ColorId");

                    b.ToTable("Buttons");
                });

            modelBuilder.Entity("LandingPage.Models.Company", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BrandSectionId");

                    b.Property<int>("LogoId");

                    b.Property<string>("Name");

                    b.Property<int>("Order");

                    b.Property<bool>("Visible");

                    b.HasKey("Id");

                    b.HasIndex("BrandSectionId");

                    b.HasIndex("LogoId")
                        .IsUnique();

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("LandingPage.Models.ContactUsSection", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Building");

                    b.Property<string>("City");

                    b.Property<string>("Country");

                    b.Property<string>("Email");

                    b.Property<string>("Heading");

                    b.Property<string>("Phone");

                    b.Property<int>("SectionId");

                    b.Property<string>("Street");

                    b.Property<string>("Subheading");

                    b.HasKey("Id");

                    b.HasIndex("SectionId")
                        .IsUnique();

                    b.ToTable("ContactUsSection");
                });

            modelBuilder.Entity("LandingPage.Models.Feature", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<int>("FeatureSectionId");

                    b.Property<int>("ImageId");

                    b.Property<int>("Order");

                    b.Property<string>("Title");

                    b.Property<bool>("Visible");

                    b.HasKey("Id");

                    b.HasIndex("FeatureSectionId");

                    b.HasIndex("ImageId");

                    b.ToTable("Features");
                });

            modelBuilder.Entity("LandingPage.Models.FeatureSection", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Heading");

                    b.Property<int>("SectionId");

                    b.Property<string>("Subheading");

                    b.HasKey("Id");

                    b.HasIndex("SectionId")
                        .IsUnique();

                    b.ToTable("FeatureSections");
                });

            modelBuilder.Entity("LandingPage.Models.Footer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ButtonId");

                    b.Property<string>("CopyrightNotice");

                    b.Property<int>("SectionId");

                    b.HasKey("Id");

                    b.HasIndex("ButtonId")
                        .IsUnique();

                    b.HasIndex("SectionId")
                        .IsUnique();

                    b.ToTable("Footers");
                });

            modelBuilder.Entity("LandingPage.Models.Header", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ButtonId");

                    b.Property<int?>("LogoImgId");

                    b.Property<string>("LogoText");

                    b.Property<int>("SectionId");

                    b.HasKey("Id");

                    b.HasIndex("ButtonId")
                        .IsUnique();

                    b.HasIndex("LogoImgId")
                        .IsUnique();

                    b.HasIndex("SectionId")
                        .IsUnique();

                    b.ToTable("Headers");
                });

            modelBuilder.Entity("LandingPage.Models.HeadSection", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("SectionId");

                    b.HasKey("Id");

                    b.HasIndex("SectionId")
                        .IsUnique();

                    b.ToTable("HeadSections");
                });

            modelBuilder.Entity("LandingPage.Models.Image", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Keywords");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Url")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Images");
                });

            modelBuilder.Entity("LandingPage.Models.NavLink", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("FooterId");

                    b.Property<int?>("HeaderId");

                    b.Property<int>("Order");

                    b.Property<string>("Title");

                    b.Property<string>("Url");

                    b.Property<bool>("VisibleInFooter");

                    b.Property<bool>("VisibleInHeader");

                    b.HasKey("Id");

                    b.HasIndex("FooterId");

                    b.HasIndex("HeaderId");

                    b.ToTable("NavLinks");
                });

            modelBuilder.Entity("LandingPage.Models.PortfolioSection", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Heading");

                    b.Property<int>("SectionId");

                    b.Property<string>("Subheading");

                    b.HasKey("Id");

                    b.HasIndex("SectionId")
                        .IsUnique();

                    b.ToTable("PortfolioSections");
                });

            modelBuilder.Entity("LandingPage.Models.Post", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Article");

                    b.Property<int>("BlogId");

                    b.Property<bool>("Featured");

                    b.Property<int>("ImageId");

                    b.Property<DateTime>("PostedOn");

                    b.Property<string>("Summary");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.HasIndex("BlogId");

                    b.HasIndex("ImageId");

                    b.ToTable("Posts");
                });

            modelBuilder.Entity("LandingPage.Models.Project", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("BackgroundColorId");

                    b.Property<int?>("BackgroundImageId");

                    b.Property<string>("Description");

                    b.Property<int>("Order");

                    b.Property<int>("PortfolioSectionId");

                    b.Property<int>("ScreenshotId");

                    b.Property<string>("Title");

                    b.Property<bool>("Visible");

                    b.HasKey("Id");

                    b.HasIndex("BackgroundColorId");

                    b.HasIndex("BackgroundImageId");

                    b.HasIndex("PortfolioSectionId");

                    b.HasIndex("ScreenshotId")
                        .IsUnique();

                    b.ToTable("Projects");
                });

            modelBuilder.Entity("LandingPage.Models.Section", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("BackgroundColorId");

                    b.Property<int?>("BackgroundImageId");

                    b.Property<string>("Description");

                    b.Property<int>("SectionType");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.HasIndex("BackgroundColorId");

                    b.HasIndex("BackgroundImageId");

                    b.ToTable("Sections");
                });

            modelBuilder.Entity("LandingPage.Models.Service", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ButtonId");

                    b.Property<string>("Description");

                    b.Property<int>("IconId");

                    b.Property<int>("ImageId");

                    b.Property<int>("Order");

                    b.Property<int>("ServiceSectionId");

                    b.Property<string>("Title");

                    b.Property<bool>("Visible");

                    b.HasKey("Id");

                    b.HasIndex("ButtonId");

                    b.HasIndex("IconId");

                    b.HasIndex("ImageId");

                    b.HasIndex("ServiceSectionId");

                    b.ToTable("Services");
                });

            modelBuilder.Entity("LandingPage.Models.ServiceSection", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Heading");

                    b.Property<int>("SectionId");

                    b.Property<string>("Subheading");

                    b.HasKey("Id");

                    b.HasIndex("SectionId")
                        .IsUnique();

                    b.ToTable("ServiceSections");
                });

            modelBuilder.Entity("LandingPage.Models.TeamMember", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ImageId");

                    b.Property<string>("Name");

                    b.Property<int>("Order");

                    b.Property<string>("Position");

                    b.Property<int>("TeamSectionId");

                    b.Property<bool>("Visible");

                    b.HasKey("Id");

                    b.HasIndex("ImageId")
                        .IsUnique();

                    b.HasIndex("TeamSectionId");

                    b.ToTable("TeamMembers");
                });

            modelBuilder.Entity("LandingPage.Models.TeamSection", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Heading");

                    b.Property<int>("SectionId");

                    b.Property<string>("Subheading");

                    b.HasKey("Id");

                    b.HasIndex("SectionId")
                        .IsUnique();

                    b.ToTable("TeamSections");
                });

            modelBuilder.Entity("LandingPage.Models.Testimonial", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AuthorName");

                    b.Property<int>("AuthorPhotoId");

                    b.Property<string>("AuthorPosition");

                    b.Property<int>("CompanyId");

                    b.Property<int>("Order");

                    b.Property<string>("Recommendation");

                    b.Property<int>("TestimonialSectionId");

                    b.Property<bool>("Visible");

                    b.HasKey("Id");

                    b.HasIndex("AuthorPhotoId");

                    b.HasIndex("CompanyId");

                    b.HasIndex("TestimonialSectionId");

                    b.ToTable("Testimonials");
                });

            modelBuilder.Entity("LandingPage.Models.TestimonialSection", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Heading");

                    b.Property<int>("SectionId");

                    b.Property<string>("Subheading");

                    b.HasKey("Id");

                    b.HasIndex("SectionId")
                        .IsUnique();

                    b.ToTable("TestimonialSections");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("LandingPage.Models.Banner", b =>
                {
                    b.HasOne("LandingPage.Color", "BackgroundColor")
                        .WithMany("Banners")
                        .HasForeignKey("BackgroundColorId");

                    b.HasOne("LandingPage.Models.Image", "BackgroundImage")
                        .WithMany("BannerBackgrounds")
                        .HasForeignKey("BackgroundImageId");

                    b.HasOne("LandingPage.Models.HeadSection", "HeadSection")
                        .WithMany("Banners")
                        .HasForeignKey("HeadSectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.Blog", b =>
                {
                    b.HasOne("LandingPage.Models.Button", "Button")
                        .WithOne("Blog")
                        .HasForeignKey("LandingPage.Models.Blog", "ButtonId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.Footer", "Footer")
                        .WithOne("Blog")
                        .HasForeignKey("LandingPage.Models.Blog", "FooterId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.Header", "Header")
                        .WithOne("Blog")
                        .HasForeignKey("LandingPage.Models.Blog", "HeaderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.BrandSection", b =>
                {
                    b.HasOne("LandingPage.Models.Section", "Section")
                        .WithOne("BrandSection")
                        .HasForeignKey("LandingPage.Models.BrandSection", "SectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.Button", b =>
                {
                    b.HasOne("LandingPage.Color", "Color")
                        .WithMany("Buttons")
                        .HasForeignKey("ColorId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.Company", b =>
                {
                    b.HasOne("LandingPage.Models.BrandSection", "BrandSection")
                        .WithMany("Companies")
                        .HasForeignKey("BrandSectionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.Image", "Logo")
                        .WithOne("CompanyLogo")
                        .HasForeignKey("LandingPage.Models.Company", "LogoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.ContactUsSection", b =>
                {
                    b.HasOne("LandingPage.Models.Section", "Section")
                        .WithOne("ContactUsSection")
                        .HasForeignKey("LandingPage.Models.ContactUsSection", "SectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.Feature", b =>
                {
                    b.HasOne("LandingPage.Models.FeatureSection", "FeatureSection")
                        .WithMany("Features")
                        .HasForeignKey("FeatureSectionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.Image", "Image")
                        .WithMany("Features")
                        .HasForeignKey("ImageId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.FeatureSection", b =>
                {
                    b.HasOne("LandingPage.Models.Section", "Section")
                        .WithOne("FeatureSection")
                        .HasForeignKey("LandingPage.Models.FeatureSection", "SectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.Footer", b =>
                {
                    b.HasOne("LandingPage.Models.Button", "Button")
                        .WithOne("Footer")
                        .HasForeignKey("LandingPage.Models.Footer", "ButtonId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.Section", "Section")
                        .WithOne("Footer")
                        .HasForeignKey("LandingPage.Models.Footer", "SectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.Header", b =>
                {
                    b.HasOne("LandingPage.Models.Button", "Button")
                        .WithOne("Header")
                        .HasForeignKey("LandingPage.Models.Header", "ButtonId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.Image", "LogoImg")
                        .WithOne("Header")
                        .HasForeignKey("LandingPage.Models.Header", "LogoImgId");

                    b.HasOne("LandingPage.Models.Section", "Section")
                        .WithOne("Header")
                        .HasForeignKey("LandingPage.Models.Header", "SectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.HeadSection", b =>
                {
                    b.HasOne("LandingPage.Models.Section", "Section")
                        .WithOne("HeadSection")
                        .HasForeignKey("LandingPage.Models.HeadSection", "SectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.NavLink", b =>
                {
                    b.HasOne("LandingPage.Models.Footer", "Footer")
                        .WithMany("NavLinks")
                        .HasForeignKey("FooterId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("LandingPage.Models.Header", "Header")
                        .WithMany("NavLinks")
                        .HasForeignKey("HeaderId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("LandingPage.Models.PortfolioSection", b =>
                {
                    b.HasOne("LandingPage.Models.Section", "Section")
                        .WithOne("PortfolioSection")
                        .HasForeignKey("LandingPage.Models.PortfolioSection", "SectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.Post", b =>
                {
                    b.HasOne("LandingPage.Models.Blog", "Blog")
                        .WithMany("Posts")
                        .HasForeignKey("BlogId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.Image", "Image")
                        .WithMany("Posts")
                        .HasForeignKey("ImageId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.Project", b =>
                {
                    b.HasOne("LandingPage.Color", "BackgroundColor")
                        .WithMany("Projects")
                        .HasForeignKey("BackgroundColorId");

                    b.HasOne("LandingPage.Models.Image", "BackgroundImage")
                        .WithMany("ProjectBackgrounds")
                        .HasForeignKey("BackgroundImageId");

                    b.HasOne("LandingPage.Models.PortfolioSection", "PortfolioSection")
                        .WithMany("Projects")
                        .HasForeignKey("PortfolioSectionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.Image", "Screenshot")
                        .WithOne("ProjectScreenshot")
                        .HasForeignKey("LandingPage.Models.Project", "ScreenshotId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.Section", b =>
                {
                    b.HasOne("LandingPage.Color", "BackgroundColor")
                        .WithMany("Sections")
                        .HasForeignKey("BackgroundColorId");

                    b.HasOne("LandingPage.Models.Image", "BackgroundImage")
                        .WithMany("Sections")
                        .HasForeignKey("BackgroundImageId");
                });

            modelBuilder.Entity("LandingPage.Models.Service", b =>
                {
                    b.HasOne("LandingPage.Models.Button", "Button")
                        .WithMany("Services")
                        .HasForeignKey("ButtonId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.Image", "Icon")
                        .WithMany("ServiceIcons")
                        .HasForeignKey("IconId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.Image", "Image")
                        .WithMany("ServiceImages")
                        .HasForeignKey("ImageId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.ServiceSection", "ServiceSection")
                        .WithMany("Services")
                        .HasForeignKey("ServiceSectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.ServiceSection", b =>
                {
                    b.HasOne("LandingPage.Models.Section", "Section")
                        .WithOne("ServiceSection")
                        .HasForeignKey("LandingPage.Models.ServiceSection", "SectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.TeamMember", b =>
                {
                    b.HasOne("LandingPage.Models.Image", "Image")
                        .WithOne("TeamMember")
                        .HasForeignKey("LandingPage.Models.TeamMember", "ImageId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.TeamSection", "TeamSection")
                        .WithMany("TeamMembers")
                        .HasForeignKey("TeamSectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.TeamSection", b =>
                {
                    b.HasOne("LandingPage.Models.Section", "Section")
                        .WithOne("TeamSection")
                        .HasForeignKey("LandingPage.Models.TeamSection", "SectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.Testimonial", b =>
                {
                    b.HasOne("LandingPage.Models.Image", "AuthorPhoto")
                        .WithMany("Testimonials")
                        .HasForeignKey("AuthorPhotoId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.Company", "Company")
                        .WithMany("Testimonials")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.TestimonialSection", "TestimonialSection")
                        .WithMany("Testimonials")
                        .HasForeignKey("TestimonialSectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LandingPage.Models.TestimonialSection", b =>
                {
                    b.HasOne("LandingPage.Models.Section", "Section")
                        .WithOne("TestimonialSection")
                        .HasForeignKey("LandingPage.Models.TestimonialSection", "SectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("LandingPage.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("LandingPage.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LandingPage.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
