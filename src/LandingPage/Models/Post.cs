﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Article { get; set; }
        public DateTime PostedOn { get; set; }
        public bool Featured { get; set; }

        public Image Image{get; set;}
        public int ImageId { get; set; }

        public Blog Blog { get; set; }
        public int BlogId { get; set; }
    }
}
