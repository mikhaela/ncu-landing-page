﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class Banner
    {
        public int Id { get; set; }
        public string Heading { get; set; }
        public string Subheading { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }

        public HeadSection HeadSection { get; set; }
        public int HeadSectionId { get; set; }

        public Image BackgroundImage { get; set; }
        public int? BackgroundImageId { get; set; }

        public Color BackgroundColor { get; set; }
        public int? BackgroundColorId { get; set; }
    }
}
