﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class TestimonialSection : ISection
    {
        public int Id { get; set; }
        public string Heading { get; set; }
        public string Subheading { get; set; }

        public Section Section { get; set; }
        public int SectionId { get; set; }

        public List<Testimonial> Testimonials { get; set; }
    }
}
