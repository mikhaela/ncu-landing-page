﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class Footer :ISection
    {
        public int Id { get; set; }
        public string CopyrightNotice { get; set; }

        public List<NavLink> NavLinks { get; set; }

        public Button Button { get; set; }
        public int ButtonId { get; set; }

        public Section Section { get; set; }
        public int SectionId { get; set; }

        public Blog Blog { get; set; }
    }
}
