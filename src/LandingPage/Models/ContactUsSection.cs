﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class ContactUsSection :ISection
    {
        public int Id { get; set; }
        public string Heading { get; set; }
        public string Subheading { get; set; }
        public string Building { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public Section Section { get; set; }
        public int SectionId { get; set; }
    }
}
