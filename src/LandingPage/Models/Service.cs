﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    // colors for tab background for tab button should be added
    public class Service
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }

        [InverseProperty("ServiceImages")]
        public Image Image { get; set; }
        public int ImageId { get; set; }

        [InverseProperty("ServiceIcons")]
        public Image Icon { get; set; }
        public int IconId { get; set; }

        public Button Button { get; set; }
        public int ButtonId { get; set; }
        
        public ServiceSection ServiceSection { get; set; }
        public int ServiceSectionId { get; set; }
    }
}
