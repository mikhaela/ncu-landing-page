﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }

        public Color BackgroundColor { get; set; }
        public int? BackgroundColorId { get; set; }

        [InverseProperty("ProjectBackgrounds")]
        public Image BackgroundImage { get; set; }
        public int? BackgroundImageId { get; set; }

        [InverseProperty("ProjectScreenshot")]
        public Image Screenshot { get; set; }
        public int ScreenshotId { get; set; }
        
        public PortfolioSection PortfolioSection { get; set; }
        public int PortfolioSectionId { get; set; }
    }
}
