﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class NavLink
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public int Order { get; set; }
        public bool VisibleInFooter { get; set; }
        public bool VisibleInHeader { get; set; }

        public Footer Footer { get; set; }
        public int? FooterId { get; set; }

        public Header Header { get; set; }
        public int? HeaderId { get; set; }
    }
}
