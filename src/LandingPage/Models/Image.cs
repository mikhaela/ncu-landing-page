﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class Image
    {
        public int Id { get; set; }
        [Required] // needs unique check
        public string Name { get; set; }
        [Required] // needs unique check
        public string Url { get; set; }
        public string Keywords { get; set; }

        public List<Section> Sections { get; set; }

        public List<Banner> BannerBackgrounds { get; set; }

        [InverseProperty("BackgroundImage")]
        public List<Project> ProjectBackgrounds { get; set; }

        [InverseProperty("Screenshot")]
        public Project ProjectScreenshot { get; set; }

        public Company CompanyLogo { get; set; }

        public List<Testimonial> Testimonials { get; set; }

        public List<Feature> Features { get; set; }

        public List<Post> Posts { get; set; }

        public Header Header { get; set; }

        [InverseProperty("Image")]
        public List<Service> ServiceImages { get; set; }

        [InverseProperty("Icon")]
        public List<Service> ServiceIcons { get; set; }

        public TeamMember TeamMember { get; set; }
    }
}
