﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class Feature
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }

        public Image Image { get; set; }
        public int ImageId { get; set; }

        public FeatureSection FeatureSection { get; set; }
        public int FeatureSectionId { get; set; }
    }
}
