﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class HeadSection : ISection
    {
        public int Id { get; set; }

        public Section Section { get; set; }
        public int SectionId { get; set; }
        
        public List<Banner> Banners { get; set; }
    }
}
