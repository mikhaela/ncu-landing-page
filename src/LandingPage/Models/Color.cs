﻿using LandingPage.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage
{
    public class Color
    {
        public int Id { get; set; }
        [Required] // needs unique check
        public string Name { get; set; }
        [Required] // needs unique check
        public string Value { get; set; }

        [InverseProperty("BackgroundColor")]
        public List<Section> Sections { get; set; }

        public List<Banner> Banners { get; set; }

        public List<Button> Buttons { get; set; }

        public List<Project> Projects { get; set; }
    }
}
