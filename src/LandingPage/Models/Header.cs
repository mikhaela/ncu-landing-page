﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class Header :ISection
    {
        public int Id { get; set; }

        public string LogoText { get; set; }

        public Image LogoImg { get; set; }
        public int? LogoImgId { get; set; }

        public List<NavLink> NavLinks { get; set; }

        public Button Button { get; set; }
        public int ButtonId { get; set; }

        public Section Section { get; set; }
        public int SectionId { get; set; }

        public Blog Blog { get; set; }
    }
}
