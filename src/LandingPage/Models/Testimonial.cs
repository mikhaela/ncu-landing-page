﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class Testimonial
    {
        public int Id { get; set; }
        public string Recommendation { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }
        public string AuthorName { get; set; } 
        public string AuthorPosition { get; set; }

        public Image AuthorPhoto { get; set; }
        public int AuthorPhotoId { get; set; }

        public Company Company { get; set; }
        public int CompanyId { get; set; }

        public TestimonialSection TestimonialSection { get; set; }
        public int TestimonialSectionId { get; set; }
    }
}
