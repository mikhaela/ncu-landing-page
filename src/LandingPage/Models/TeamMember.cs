﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class TeamMember
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }

        public Image Image { get; set; }
        public int ImageId { get; set; }

        public TeamSection TeamSection { get; set; }
        public int TeamSectionId { get; set; }
    }
}
