﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class FeatureSection :ISection
    {
        public int Id { get; set; }
        public string Heading { get; set; }
        public string Subheading { get; set; }

        public List<Feature> Features { get; set; }

        public Section Section { get; set; }
        public int SectionId { get; set; }
    }
}
