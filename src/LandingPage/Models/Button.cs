﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class Button
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Action { get; set; }
        public string LabelForButton { get; set; }
        public bool Visible { get; set; }

        public Color Color { get; set; }
        public int ColorId { get; set; }

        public List<Service> Services { get; set; }

        public Header Header { get; set; }

        public Footer Footer { get; set; }

        public Blog Blog { get; set; }
    }
}
