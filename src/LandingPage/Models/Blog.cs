﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class Blog
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public Header Header { get; set; }
        public int HeaderId { get; set; }

        public Footer Footer { get; set; }
        public int FooterId { get; set; }

        public Button Button { get; set; }
        public int ButtonId { get; set; }

        public List<Post> Posts { get; set; } 
    }
}
