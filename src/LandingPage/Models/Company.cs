﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }

        public Image Logo { get; set; }
        public int LogoId { get; set; }

        public BrandSection BrandSection { get; set; }
        public int BrandSectionId { get; set; }

        [InverseProperty("Company")]
        public List<Testimonial> Testimonials { get; set; }
    }
}
