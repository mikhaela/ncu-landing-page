﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Models
{
    public enum SectionType
    {
        Header = 1,
        Head,
        Team,
        Service,
        Feature,
        Testimonial,
        Brand,
        Portfolio,
        ContactUs,
        Footer
    }

    public class Section
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public SectionType SectionType { get; set; }

        public Color BackgroundColor { get; set; }
        public int? BackgroundColorId { get; set; }

        public Image BackgroundImage { get; set; }
        public int? BackgroundImageId { get; set; }

        public HeadSection HeadSection { get; set; }

        public ContactUsSection ContactUsSection { get; set; }

        public PortfolioSection PortfolioSection { get; set; }

        public BrandSection BrandSection { get; set; }

        public TestimonialSection TestimonialSection { get; set; }

        public FeatureSection FeatureSection { get; set; }

        public TeamSection TeamSection { get; set; }

        public ServiceSection ServiceSection { get; set; }

        public Header Header { get; set; }

        public Footer Footer { get; set; }
    }
}
