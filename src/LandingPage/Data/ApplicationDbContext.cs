﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using LandingPage.Models;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LandingPage.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Section> Sections { get; set; }
        public DbSet<BrandSection> BrandSections { get; set; }
        public DbSet<ContactUsSection> ContactUsSection { get; set; }
        public DbSet<FeatureSection> FeatureSections { get; set; }
        public DbSet<Footer> Footers { get; set; }
        public DbSet<Header> Headers { get; set; }
        public DbSet<HeadSection> HeadSections { get; set; }
        public DbSet<PortfolioSection> PortfolioSections { get; set; }
        public DbSet<TeamSection> TeamSections { get; set; }
        public DbSet<TestimonialSection> TestimonialSections { get; set; }
        public DbSet<ServiceSection> ServiceSections { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Banner> Banners { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Testimonial> Testimonials { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<TeamMember> TeamMembers { get; set; }
        public DbSet<Button> Buttons { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<NavLink> NavLinks { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<NavLink>()
                .HasOne(p => p.Header)
                .WithMany(b => b.NavLinks)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<NavLink>()
                .HasOne(p => p.Footer)
                .WithMany(b => b.NavLinks)
                .OnDelete(DeleteBehavior.SetNull);

            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
